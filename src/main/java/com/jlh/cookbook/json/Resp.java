package com.jlh.cookbook.json;

/**
 * com.jlh.cookbook.json
 * Created by ASUS on 2017/3/4.
 * 14:14
 */
public interface Resp {
    String data="{\n" +
            "    \"resultcode\": \"200\",\n" +
            "    \"reason\": \"Success\",\n" +
            "    \"result\": {\n" +
            "        \"data\": [\n" +
            "            {\n" +
            "                \"id\": \"7395\",\n" +
            "                \"title\": \"蚂蚁上树\",\n" +
            "                \"tags\": \"川菜;咸香;青少年;白领\",\n" +
            "                \"imtro\": \"一直不明白为什么把粉条炒肉沫叫做蚂蚁上树，今天做完菜去百度一下才知道还有个传说，这道菜的由来据说与元代剧作家关汉卿笔下的人物窦娥有关。具体故事太长我就不讲了，先给大家说说我做菜的过程吧\",\n" +
            "                \"ingredients\": \"植物油,150g;粉丝,200g;肉沫,80g\",\n" +
            "                \"burden\": \"葱,适量;豆瓣酱,30g;汤,150g;干辣椒,30g;姜,适量;蒜瓣,适量\",\n" +
            "                \"albums\": [\n" +
            "                    \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/t/8/7395_148666.jpg\"\n" +
            "                ],\n" +
            "                \"steps\": [\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/74/7395_beed8bb9968df36a.jpg\",\n" +
            "                        \"step\": \"1.用冷水将粉丝提前浸泡半小时。（大致有些软就可以了）\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/74/7395_09c9d51f5b538745.jpg\",\n" +
            "                        \"step\": \"2.锅内冷油，放入姜蓉，蒜蓉爆香，.放入肉沫煸炒至熟\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/74/7395_8ff7a7fd0025d59d.jpg\",\n" +
            "                        \"step\": \"3.放入豆瓣酱2/3大匙,料酒1大匙，炒至出红油，.放入泡软粉丝。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/74/7395_91ce17b750c602fa.jpg\",\n" +
            "                        \"step\": \"4.倒入高汤2/3杯，生抽1/2大匙，中火煮开后，转小火煮至水即干未干的状态。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/74/7395_370aeb9baf3e98fb.jpg\",\n" +
            "                        \"step\": \"5.最后临出锅前撒上些香菜葱花就行了。\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"7401\",\n" +
            "                \"title\": \"水煮牛肉\",\n" +
            "                \"tags\": \"川菜;家常菜;热菜;健脾开胃;补血;增肥;阳虚质;贫血;骨质疏松;辣;10-20分钟;煮;简单;豆豉;凉拌;补钙;麻;提高免疫力;补铁;1-2人;健脾;祛风散寒;祛寒;补虚;锅子;中等难度;1小时-2小时;强筋健骨;增高;脾虚\",\n" +
            "                \"imtro\": \"水煮牛肉一直是餐馆中广受欢迎的一道菜，但是其实在家里做也非常容易，最关键的是比餐馆中更实惠，吃的更放心。\",\n" +
            "                \"ingredients\": \"牛肉,350g;白菜,100g;豆芽,100g\",\n" +
            "                \"burden\": \"青葱,2根;辣椒,适量;花椒,适量;豆瓣酱,1.5勺;豆豉,适量\",\n" +
            "                \"albums\": [\n" +
            "                    \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/t/8/7401_189475.jpg\"\n" +
            "                ],\n" +
            "                \"steps\": [\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/74/7401_82f88e61d2b1f9d3.jpg\",\n" +
            "                        \"step\": \"1.白菜切段、肉切片，切下来的牛肉片，加上水淀粉、一小勺盐和料酒，可以放一点点老抽（用来上色的），淀粉要稍多一些让肉片在嘴里感觉很嫩滑。和匀了浆上30分钟。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/74/7401_abed80e68d1b5678.jpg\",\n" +
            "                        \"step\": \"2.炒辣椒花椒。锅置火上，烧干水汽，关小火将干辣椒和花椒倒入翻炒。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/74/7401_65707246341a8fe0.jpg\",\n" +
            "                        \"step\": \"3.辣椒花椒碾碎。将上面煸香的辣椒和花椒盛出，辣椒切成小段，花椒用刀来回碾碎。老姜、葱白一小段、蒜四瓣。姜葱蒜都切成片。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/74/7401_6099a6280b0074d6.jpg\",\n" +
            "                        \"step\": \"4.炒制佐料。锅内放油，油热将豆瓣酱，豆豉倒入炒香，然后放入姜蒜葱炒一小会，加水做汤。炒好的佐料加水，放盐、味精，青蒜。煮开。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/74/7401_417ef86be72e6e00.jpg\",\n" +
            "                        \"step\": \"5.另起锅把白菜豆芽加豆瓣酱稍微炒一下。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/74/7401_318a1fd0ef5ac0d2.jpg\",\n" +
            "                        \"step\": \"6.煮肉。关小火，临下锅前再将牛肉片搅一搅，然后将浆好的牛肉逐片划入，全部入锅以后将火开大些。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/74/7401_0618583be13a2abc.jpg\",\n" +
            "                        \"step\": \"7.将煮好的牛肉连汤倒入碗中，撒上辣椒和花椒碎，我方了点芝麻。把锅洗净，锅内放油，油热后倒入碗中。\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"7408\",\n" +
            "                \"title\": \"泡椒凤爪\",\n" +
            "                \"tags\": \"川菜;家常菜;健脾开胃;青少年;白领;全菜系;1-2人;泡椒味;锅子;1小时-2小时\",\n" +
            "                \"imtro\": \"这道菜是很多人都喜欢的小吃，看电视的时候吃上一个别提有多惬意了，自己做其实也非常的容易，大家可以试试。\",\n" +
            "                \"ingredients\": \"凤爪,300g;泡椒,100g;洋葱,50g;芹菜,50g\",\n" +
            "                \"burden\": \"干辣椒,适量;花椒,适量;大蒜,适量;姜,适量;白酒,适量;白醋,适量;鸡粉,适量;盐,适量;味精,适量;糖,适量\",\n" +
            "                \"albums\": [\n" +
            "                    \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/t/8/7408_306903.jpg\"\n" +
            "                ],\n" +
            "                \"steps\": [\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7408_553772d9121f82bf.jpg\",\n" +
            "                        \"step\": \"1.鸡爪去指甲，洗干净。锅内烧热水，放人鸡爪煮，加2片姜为好。不要猪太久，劲道的比较好吃。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7408_8709419298e8c732.jpg\",\n" +
            "                        \"step\": \"2.把煮好的鸡爪用水反复冲洗，准备一盆冷水和一快冰，浸泡鸡爪1个小时。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7408_551f069f90e1138b.jpg\",\n" +
            "                        \"step\": \"3.把洋葱切片，姜切片，蒜，芹菜切条，干辣椒捏碎，泡椒最好剪小颗粒，容易入味放入坛中，加白醋，白酒，少量水，鸡粉，盐，味精，糖，花椒。最好多放点盐，容易入味。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7408_6232387a69ad868c.jpg\",\n" +
            "                        \"step\": \"4.泡1天就可以吃了，3天吃味道更浓后。\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"7732\",\n" +
            "                \"title\": \"宫保鸡丁\",\n" +
            "                \"tags\": \"川菜;家常菜;咸香;朋友聚餐;其他工艺\",\n" +
            "                \"imtro\": \"上次做过一道饭店点击率最高的菜---鱼香肉丝，很多朋友给我留言说非常好吃，所以今天咱们继续做宫保鸡丁。这个菜的关键在于宫保汁，只要掌握好调料比例，咱也能轻松做出饭店级的味道！\",\n" +
            "                \"ingredients\": \"鸡腿肉,3块;鸡蛋,1个;花生米,50g\",\n" +
            "                \"burden\": \"大葱,3根;料酒,30ml;玉米淀粉,45g;盐,5g;干辣椒,6个;花椒,10g;酱油,15ml;香醋,8ml;白糖,15g\",\n" +
            "                \"albums\": [\n" +
            "                    \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/t/8/7732_736529.jpg\"\n" +
            "                ],\n" +
            "                \"steps\": [\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7732_8c9d017fcb41499b.jpg\",\n" +
            "                        \"step\": \"1.鸡腿肉去骨后切成1.5厘米见方的块，加入盐5克、鸡蛋1个、料酒、水淀粉15ml拌匀，腌制半小时。大葱切成2厘米长的段。然后取一小碗，将宫保酱汁的原料依次加入，调匀后备用；\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7732_e1b373939990263c.jpg\",\n" +
            "                        \"step\": \"2.锅中加入少许油，大火加热到4成热时放入腌好的鸡腿肉，快速翻炒，看到肉质开始变白后立即捞出；\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7732_b2a038c1e19956c0.jpg\",\n" +
            "                        \"step\": \"3.锅中重新加入底油烧到5成热，放入花椒粒炸出香味，然后捞出不用，再放入干辣椒、大葱节炒出香味；\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7732_a50743a5dc25c338.jpg\",\n" +
            "                        \"step\": \"4.然后倒入鸡腿肉，将宫保酱汁搅匀后立即划圈淋入锅中，快速翻炒均匀，看到鸡腿肉上色后，撒上炸花生米，拌匀后立即出锅。\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"7482\",\n" +
            "                \"title\": \"鱼香肉丝\",\n" +
            "                \"tags\": \"川菜;家常菜;瘦身;炒;香;冬季;减肥;补铁;补中益气;肥胖;炒锅\",\n" +
            "                \"imtro\": \"这道菜非常下饭，喜欢的可以试试做来吃，味道很不错哦。\",\n" +
            "                \"ingredients\": \"猪肉,150g;冬笋,150g\",\n" +
            "                \"burden\": \"木耳,适量;泡辣椒,适量;蒜苔,适量;酱油,适量;醋,适量;糖,适量;味精,适量;淀粉,适量;盐,适量;姜,适量;蒜,适量;葱,适量\",\n" +
            "                \"albums\": [\n" +
            "                    \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/t/8/7482_123337.jpg\"\n" +
            "                ],\n" +
            "                \"steps\": [\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7482_92f789a37f3af3bf.jpg\",\n" +
            "                        \"step\": \"1.把里脊肉切成6厘米长、宽厚各0.2厘米的丝，用酒、盐，水淀粉拌和均匀。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7482_6ac4420c3be4d4b1.jpg\",\n" +
            "                        \"step\": \"2.另将白糖、酱油、醋、味精、淀粉，料酒，十三香粉调在小碗里待用。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7482_ef506c8f433250a9.jpg\",\n" +
            "                        \"step\": \"3.锅烧到六成热时，放入生油，随即倒入肉丝炒散，视断血端锅倒入漏勺，沥去油。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7482_126f0025bbc99912.jpg\",\n" +
            "                        \"step\": \"4.笋丝、木耳洗净，切成丝，泡红辣椒剁细；姜、蒜切细末，葱切成花。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7482_41cc423f9b05100c.jpg\",\n" +
            "                        \"step\": \"5.加泡辣椒、姜、蒜末炒出香味，再放。木耳、笋丝、葱花炒匀。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7482_83269458b492db1d.jpg\",\n" +
            "                        \"step\": \"6.烹入芡汁，迅速翻簸起锅装盘即成。\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"7524\",\n" +
            "                \"title\": \"啤酒鸭\",\n" +
            "                \"tags\": \"锅子;半小时-1小时;1-2人;川菜;香辣;家常菜;下酒菜;晚餐\",\n" +
            "                \"imtro\": \"有啤酒的香味～\",\n" +
            "                \"ingredients\": \"鸭,半只;啤酒,1厅\",\n" +
            "                \"burden\": \"糖,1大勺;桂皮,1小块;香叶,3片;水淀粉,少许;八角,2个;姜片,5片;干椒,适量;蒜,5瓣;萝卜,半个;酱油,2大匙\",\n" +
            "                \"albums\": [\n" +
            "                    \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/t/8/7524_418419.jpg\"\n" +
            "                ],\n" +
            "                \"steps\": [\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/76/7524_2ba409cd536b357c.jpg\",\n" +
            "                        \"step\": \"1.锅中下少许油烧热，然后放入鸭肉炒出油\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/76/7524_dd096c80d931cf75.jpg\",\n" +
            "                        \"step\": \"2.然后放入桂皮，香叶，八角，干椒炒出香味后倒入啤酒和萝卜，盖上锅盖焖煮\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/76/7524_2f2110ad6b01b2e3.jpg\",\n" +
            "                        \"step\": \"3.待锅中啤酒剩原来的1/2时加入酱油和糖，继续焖煮\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/76/7524_5656ad823b8b2841.jpg\",\n" +
            "                        \"step\": \"4.煮至啤酒剩原来的4/5时，加入水淀粉勾芡至汤汁收干后即可出锅\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"7458\",\n" +
            "                \"title\": \"蒜泥白肉\",\n" +
            "                \"tags\": \"川菜;家常菜;蒜香;10-20分钟;炒;简单;味精;午餐;晚餐;肉菜;苗;口味菜;1-2人;锅子;其他工艺;中等难度\",\n" +
            "                \"imtro\": \"蒜泥白肉，四川名菜。 选用肥瘦相连的猪腿肉， 经过烫煮、平片、 凉拌而成。特点是香辣鲜美， 蒜味浓厚，肥而不腻。 偶从小就怕肥肉，大概也有很多孩子跟俺一样～所以我把肉换成全瘦的了。。。这道菜要求把熟肉切成薄片，每50克10片左右。。。这得多好的刀工呀。。。可惜俺刀工很烂，200克的猪肉，数了数，我才切了20片，管他呢，这样偶已经很满意了。。。哈哈～\",\n" +
            "                \"ingredients\": \"猪坐臀肉,500g\",\n" +
            "                \"burden\": \"酱油,20g;白糖,5g;芝麻油,少许;辣椒油,25g;醋,15g;蒜,10g;鸡精,1g\",\n" +
            "                \"albums\": [\n" +
            "                    \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/t/8/7458_713648.jpg\"\n" +
            "                ],\n" +
            "                \"steps\": [\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7458_faf4d7d52496ea4f.jpg\",\n" +
            "                        \"step\": \"1.将坐臀肉洗净，放开水中焯一下，去除血污，蒜拍成泥。将肉放冷水锅中用大火烧开后，改用小火焖15～20min。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7458_2f772e8bb26fc19e.jpg\",\n" +
            "                        \"step\": \"2.将蒜泥，酱油，白糖，醋，鸡精拌和，待蒜泥内的蒜汁溢出与调味料起粘后，加入辣椒油和芝麻油\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7458_fbcf1ba645b4d0c3.jpg\",\n" +
            "                        \"step\": \"3.将熟肉切成薄片，每50克10片左右，熟肉切片，以见红断生，无血水为佳\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/75/7458_839991d819aba318.jpg\",\n" +
            "                        \"step\": \"4.取干净炒锅，加清水烧开，将肉放入开水中汆烫，见肉片卷曲时立即捞出，沥干水分，放入盘内，最后浇上蒜泥调味汁，凉拌后即可食用。\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"7703\",\n" +
            "                \"title\": \"酸菜鱼\",\n" +
            "                \"tags\": \"川菜;家常菜;热菜;辣;煮;简单;香\",\n" +
            "                \"imtro\": \"外出吃火锅剩了些生的鱼片打包回来，在家总不能再来吃火锅吧。酸菜鱼没试过，就试着在家弄弄，看看效果如何。\",\n" +
            "                \"ingredients\": \"鱼片,150g;酸菜,150g;火腿肠,2根\",\n" +
            "                \"burden\": \"青椒,适量;草菇,适量;姜,适量;蒜,适量;盐,适量;料酒,适量;胡椒粉,适量;鸡精,适量;红椒,适量\",\n" +
            "                \"albums\": [\n" +
            "                    \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/t/8/7703_483745.jpg\"\n" +
            "                ],\n" +
            "                \"steps\": [\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7703_c2cd445738ceb635.jpg\",\n" +
            "                        \"step\": \"1.鱼片先用蛋清搅拌一下。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7703_2da7063f137833d0.jpg\",\n" +
            "                        \"step\": \"2.准备好其他的配料。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7703_9dab6dae942473b0.jpg\",\n" +
            "                        \"step\": \"3.起油锅，爆香姜蒜、下酸菜、青红椒、草菇、火腿肠煸炒。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7703_70990e1afc48f32d.jpg\",\n" +
            "                        \"step\": \"4.加入料酒、加水盖过材料烧开。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7703_4056ac111d72d7cd.jpg\",\n" +
            "                        \"step\": \"5.待水开后，加入鱼片，下盐、胡椒粉调味。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7703_e420fa61307f52ef.jpg\",\n" +
            "                        \"step\": \"6.待水再烧开时鱼片就熟了，喜欢的话加点鸡精调味就好了。\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"7729\",\n" +
            "                \"title\": \"宫保鸡丁\",\n" +
            "                \"tags\": \"川菜;家常菜;咸香;宴请;朋友聚餐;1-2人;锅子;其他工艺;1小时-2小时\",\n" +
            "                \"imtro\": \"一直不敢尝试鱼香肉丝，宫保鸡丁这样的大众菜，因为太大众，吃过了太多种口味，好吃的，不好吃的阅过N次，敏锐的嗅觉自然会对这太大众的菜品无比挑剔。可不挑战一下，就永远不会知道自己做出的到底是什么味道。再者说，爱不爱吃是一回事，会不会做则是另外一回事，试一下吧，有餐厅的味道，也有家里的味道哦~~\",\n" +
            "                \"ingredients\": \"鸡腿肉,400g\",\n" +
            "                \"burden\": \"大葱,1根;花椒,15粒;花生,1串;姜,3片;干辣椒,6只;生抽,3勺;米醋,1勺;料酒,1勺;水,1勺;老抽,1勺;绵白糖,30g;淀粉,15克g\",\n" +
            "                \"albums\": [\n" +
            "                    \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/t/8/7729_954735.jpg\"\n" +
            "                ],\n" +
            "                \"steps\": [\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7729_f9a3866e1bd84efd.jpg\",\n" +
            "                        \"step\": \"1.琵琶腿4个，洗干净。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7729_80e9e86c0787f491.jpg\",\n" +
            "                        \"step\": \"2.葱切小段，姜切片，辣椒切小段，花椒一小把；\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7729_2c716c8660103847.jpg\",\n" +
            "                        \"step\": \"3.将琵琶腿去骨去皮，剔下的鸡皮可以扔掉， 鸡腿肉切成小丁，用一半份量的淀粉，1勺生抽，和半勺料酒，腌20分钟。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7729_9158e921a1f12fb7.jpg\",\n" +
            "                        \"step\": \"4.用2勺生抽、1.5勺米醋和所有白糖兑成汁1；另拿一只碗，将淀粉、水和料酒调成芡汁。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7729_533c1d9dc5feeaf8.jpg\",\n" +
            "                        \"step\": \"5.起锅，先倒一点油，用小火把花生米煎脆，一定注意火候不要煎糊； 锅中再倒一点油，将腌好的鸡肉倒入，翻炒至变色盛出备用，锅中留底油。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7729_14eee08b1ab3cc4a.jpg\",\n" +
            "                        \"step\": \"6.在底油中加些香油，放入花椒，小火煎至花椒变黑，出香味，将花椒扔掉，只留下油； 将葱段、姜片、辣椒段一同放入，炒出香味； 将炒好的鸡肉放入。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/78/7729_ae604e95164bdb32.jpg\",\n" +
            "                        \"step\": \"7.倒入调料汁1，翻炒均匀；再加一勺老抽上色；倒入芡汁勾芡，下花生米，翻炒两下，关火出锅。\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            },\n" +
            "            {\n" +
            "                \"id\": \"7506\",\n" +
            "                \"title\": \"酸菜鱼\",\n" +
            "                \"tags\": \"川菜;家常菜;酸;酸辣;半小时-1小时;煮;烧;香;其它豆类及制品;其它调料;午餐;普通;草;口味菜;1-2人;锅子;1小时-2小时;鲫\",\n" +
            "                \"imtro\": \"这几天天时不时会下点雨。下班前老公打电话给我说出去吃，一会来接我。 下班前突然就下起雨来，我看雨还蛮大的，骑个摩托车也不方便，就想干脆在家自己烧着吃了。我不会片鱼，这是超市买的直接片好的，一盒黑鱼片，一盒鱼骨，搞优惠，两盒才10元， 酸菜鱼佐料是不可少的，那种复合的味道自己不太好调，就买这种现成的吧。\",\n" +
            "                \"ingredients\": \"鱼,500g;酸菜,400g\",\n" +
            "                \"burden\": \"酸菜鱼佐料,适量;蛋清,适量;黄瓜,适量;魔芋,适量;葱,适量\",\n" +
            "                \"albums\": [\n" +
            "                    \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/t/8/7506_481942.jpg\"\n" +
            "                ],\n" +
            "                \"steps\": [\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/76/7506_c4768a134162ca8b.jpg\",\n" +
            "                        \"step\": \"1.先用“腌料包”里的粉粉，加料酒和半个蛋清，把鱼片和鱼骨分别腌制下；\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/76/7506_0d695b7cb6d281e2.jpg\",\n" +
            "                        \"step\": \"2.酸菜稍切一下，黄瓜切条，魔芋结清洗下；\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/76/7506_90366fe23e96377f.jpg\",\n" +
            "                        \"step\": \"3.起油锅，油热放酸菜炒，要炒出香味来，放入鱼骨炒至颜色发白，再把酱料包放进去炒，转小火，省得炒糊；那味很呛人，老公这时跑到厨房来，我们俩就都一直咳，眼泪都咳出来了。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/76/7506_c4c4145ca216b49c.jpg\",\n" +
            "                        \"step\": \"4.倒入适量的热水，下黄瓜条和魔芋煮一会儿，这时可以调味啦，放点盐、味精啥的；\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/76/7506_a236bf3840c32222.jpg\",\n" +
            "                        \"step\": \"5.将鱼片一片一片地放进去，等全部放好后，用筷子拨一下，就好起锅了，时间长了鱼片就老了。\"\n" +
            "                    },\n" +
            "                    {\n" +
            "                        \"img\": \"http://juheimg.oss-cn-hangzhou.aliyuncs.com/cookbook/s/76/7506_13c58292ec1a8d42.jpg\",\n" +
            "                        \"step\": \"6.顶上撒葱花，再浇点热油上去，一盆鲜香味美的酸菜鱼就出炉了。\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            }\n" +
            "        ],\n" +
            "        \"totalNum\": \"150\",\n" +
            "        \"pn\": \"1\",\n" +
            "        \"rn\": \"10\"\n" +
            "    },\n" +
            "    \"error_code\": 0\n" +
            "}";
}
