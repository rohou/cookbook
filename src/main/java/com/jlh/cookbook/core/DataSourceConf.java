package com.jlh.cookbook.core;

/**
 * com.jlh.cookbook.core
 * Created by ASUS on 2017/3/4.
 * 12:37
 */
public interface DataSourceConf {
    String driverClassName="com.mysql.jdbc.Driver";
    String url="jdbc:mysql://localhost:3307/cookbook";
    String userName= "root";
    String password="123456";
    Integer initialSize= 1;
    Integer minIdle = 1;
    Integer maxActive =10;
    Long maxWait = 10000L;
    Long timeBetweenEvictionRunsMillis=60000L;
    Long minEvictableIdleTimeMillis=300000L;
}
