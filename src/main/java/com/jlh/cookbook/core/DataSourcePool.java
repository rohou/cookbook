package com.jlh.cookbook.core;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidPooledConnection;

import java.sql.SQLException;

/**
 * com.jlh.cookbook.core
 * Created by ASUS on 2017/3/4.
 * 12:36
 */
public class DataSourcePool {
    private static DruidDataSource druidDataSource ;
    private static synchronized DruidDataSource getDataSourcePool() {
        if (druidDataSource==null){
            druidDataSource = new DruidDataSource();
            druidDataSource.setDriverClassName(DataSourceConf.driverClassName);
            druidDataSource.setUrl(DataSourceConf.url);
            druidDataSource.setUsername(DataSourceConf.userName);
            druidDataSource.setPassword(DataSourceConf.password);
            druidDataSource.setInitialSize(DataSourceConf.initialSize);
            druidDataSource.setMinIdle(DataSourceConf.minIdle);
            druidDataSource.setMaxActive(DataSourceConf.maxActive);
            druidDataSource.setMaxWait(DataSourceConf.maxWait);
            druidDataSource.setTimeBetweenEvictionRunsMillis(DataSourceConf.timeBetweenEvictionRunsMillis);
            druidDataSource.setMinEvictableIdleTimeMillis(DataSourceConf.minEvictableIdleTimeMillis);
        }
        return druidDataSource;
    }
    public static DruidPooledConnection getConnection (){
        try {
            return getDataSourcePool().getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
