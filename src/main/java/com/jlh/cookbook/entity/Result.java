package com.jlh.cookbook.entity;

import java.util.List;

/**
 * com.jlh.cookbook.entity
 * Created by ASUS on 2017/3/4.
 * 13:16
 */
public class Result {
    private List<Cook> data;
    private String totalNum;
    private String pn ;
    private String rn;

    public List<Cook> getData() {
        return data;
    }

    public void setData(List<Cook> data) {
        this.data = data;
    }

    public String getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(String totalNum) {
        this.totalNum = totalNum;
    }

    public String getPn() {
        return pn;
    }

    public void setPn(String pn) {
        this.pn = pn;
    }

    public String getRn() {
        return rn;
    }

    public void setRn(String rn) {
        this.rn = rn;
    }

    @Override
    public String toString() {
        return "Result{" +
                "data=" + data +
                ", totalNum='" + totalNum + '\'' +
                ", pn='" + pn + '\'' +
                ", rn='" + rn + '\'' +
                '}';
    }
}
