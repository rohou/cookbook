package com.jlh.cookbook.entity;

/**
 * com.jlh.cookbook.entity
 * Created by ASUS on 2017/3/4.
 * 13:06
 */
public class Step {
    private String img;
    private String step;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    @Override
    public String toString() {
        return "Step{" +
                "img='" + img + '\'' +
                ", step='" + step + '\'' +
                '}';
    }
}
