package com.jlh.cookbook.service;

import com.jlh.cookbook.core.RequestStatus;
import com.jlh.cookbook.utils.HttpClientUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.Map;

/**
 * com.jlh.cookbook.service
 * Created by ASUS on 2017/3/6.
 * 13:54
 */
public class SpiderService {
    public void getMenuMap(Map<String,String> menuUrl,HttpClientUtils httpClientUtils){
        Integer index= 1;
        while (true){
            String res=httpClientUtils.get("http://www.xiangha.com/caipu/x-chuancai/hot-"+index,null);
            if (res.equals(RequestStatus.STR_404))
                break;
            System.out.println(index);
            index++;
            Document document = Jsoup.parse(res);
            Elements elements=document.select(".rec_list>ul>li");
            elements.forEach(m->{
                Element element=m.select("a").first();
                if (element!=null) {
                    String title = element.attr("title");
                    String url = element.attr("href");
                    menuUrl.put(title, url);
                }
            });
        }
    }
    public void getMenuProp(Map<String,String> menuProp,Map<String,String> menuUrl,HttpClientUtils httpClientUtils){

        for (String key :menuUrl.keySet()){
            String res=httpClientUtils.get(menuUrl.get(key),null);
            if (res!=null&&!res.equals(RequestStatus.STR_404)) {
                Document doc = Jsoup.parse(res);
                Elements elements = doc.select(".rec_ing>table>tbody>tr>td");
                StringBuilder stringBuilder = new StringBuilder();
                elements.forEach(m -> {
                    Elements elements1 = m.select("div.cell");
                    Elements a = elements1.select("a.link");
                    String text = null;
                    if (a != null && a.size() > 0) {
                        text = a.text();
                    } else {
                        text = elements1.text();
                    }
                    System.out.println(text);
                    stringBuilder.append(text + ",");
                });
                stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
                menuProp.put(key, stringBuilder.toString());
            }
        }
    }

    public static void main(String[] args) {
        SpiderService spiderService = new SpiderService() ;
        HttpClientUtils httpClientUtils = new HttpClientUtils();
        HashMap<String,String> menuUrl = new HashMap<>();
        HashMap<String,String> menuProp = new HashMap<>();
        spiderService.getMenuMap (menuUrl,httpClientUtils);
        spiderService.getMenuProp(menuProp,menuUrl,httpClientUtils);
        System.out.println(menuProp);

    }
}
