package com.jlh.cookbook.dao;

import com.alibaba.druid.pool.DruidPooledConnection;
import com.alibaba.fastjson.JSON;
import com.jlh.cookbook.core.DataSourcePool;
import com.jlh.cookbook.entity.Cook;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * com.jlh.cookbook.dao
 * Created by ASUS on 2017/3/4.
 * 14:26
 */
public class CookDao {
    private DruidPooledConnection connection;

    public CookDao() {
        connection=DataSourcePool.getConnection();
    }

    public boolean insert(Cook cook){
        String sql="INSERT INTO tsys_cook VALUES(?,?,?,?,?,?,?,?)";
        PreparedStatement preparedStatement=null;
        try {
            preparedStatement=connection.prepareStatement(sql);
            preparedStatement.setString(1,cook.getId());
            preparedStatement.setString(2,cook.getTitle());
            preparedStatement.setString(3,cook.getTags());
            preparedStatement.setString(4,cook.getImtro());
            preparedStatement.setString(5,cook.getIngredients());
            preparedStatement.setString(6,cook.getBurden());
            preparedStatement.setString(7, JSON.toJSONString(cook.getAlbums()));
            preparedStatement.setString(8,JSON.toJSONString(cook.getSteps()));
            return preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (preparedStatement!=null)
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
        return false;
    }
}
